import { Component } from '@angular/core';
import { Platform,NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bookservice1',
  templateUrl: 'bookservice1.page.html',
  styleUrls: ['bookservice1.page.scss'],
})
export class Bookservice1Page {

  private currentNumberText = '1 Kg';
  private currentNumber = 1;
  constructor(
    private nav:NavController,
    private router: Router
  ) {}

  onDescrement() {
    if(this.currentNumber > 1){
      this.currentNumber--;
      this.currentNumberText = this.currentNumber+' Kg';
    }
  }

  onIncrement() {
    this.currentNumber++;
    this.currentNumberText = this.currentNumber+' Kg';
  }

  goBooking(){
    this.router.navigateByUrl('success');
  }
}