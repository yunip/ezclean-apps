import { Component } from '@angular/core';
import { Platform,NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bookservice',
  templateUrl: 'bookservice.page.html',
  styleUrls: ['bookservice.page.scss'],
})
export class BookservicePage {

  private currentNumberText = '1 Jam';
  private currentNumber = 1;
  constructor(
    private nav:NavController,
    private router: Router
  ) {}

  onDescrement() {
    if(this.currentNumber > 1){
      this.currentNumber--;
      this.currentNumberText = this.currentNumber+' Jam';
    }
  }

  onIncrement() {
    this.currentNumber++;
    this.currentNumberText = this.currentNumber+' Jam';
  }

  goBooking(){
    this.router.navigateByUrl('success');
  }
}