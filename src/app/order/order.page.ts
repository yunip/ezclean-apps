import { Component } from '@angular/core';
import { Platform,NavController } from '@ionic/angular';

@Component({
  selector: 'app-order',
  templateUrl: 'order.page.html',
  styleUrls: ['order.page.scss'],
})
export class OrderPage {
  list_data: string = "list-1";
  constructor(
    private nav:NavController,
  ) {}

  goDetail(){
    this.nav.navigateForward('detailorder');
  }
}
