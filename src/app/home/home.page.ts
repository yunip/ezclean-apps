import { Component } from '@angular/core';
import { Platform,NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private nav:NavController,
  ) {}

  goClean(){
    this.nav.navigateForward('bookservice');
  }

  goLoundry(){
    this.nav.navigateForward('bookservice1');
  }

}
